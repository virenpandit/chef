SET /P K_PORT=Please provide CI Build Server Provisioning Port: 
echo Updating jobs on Build server using port [%K_PORT%]...
knife bootstrap localhost -N ci.jenkins --ssh-user vagrant --ssh-password vagrant --ssh-port %K_PORT% --run-list "recipe[ci-aem]" --sudo
pause
