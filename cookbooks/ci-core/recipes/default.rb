#
# Cookbook Name:: cibuild
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt'
include_recipe 'java'
include_recipe 'maven'
include_recipe 'accurev'
include_recipe 'jenkins::server'

jenkins_plugin 'accurev'
jenkins_plugin 'cloudbees-folder'
jenkins_plugin 'copyartifact'
jenkins_plugin 'credentials'
jenkins_plugin 'findbugs'
jenkins_plugin 'maven-plugin'
jenkins_plugin 'analysis-core'

temp=Chef::Config[:file_cache_path]

remote_file "#{Chef::Config[:file_cache_path]}/jenkins-config.zip" do
     source "http://offbelaycorp/jenkins/jenkins-config.zip"
     mode 00777
end

remote_file "#{temp}/appserveragent.zip" do
     source "http://offbelaycorp/appserveragent.zip"
     mode 00777
end

remote_file "#{temp}/machineagent.zip" do
     source "http://offbelaycorp/machineagent.zip"
     mode 00777
end


bash 'run_jar' do
     code <<-EOF
       /etc/init.d/jenkins stop
       /usr/bin/unzip #{temp}/jenkins-config.zip -d /var/lib/jenkins

       chown -R jenkins:jenkins /var/lib/jenkins

       /etc/init.d/jenkins start



	# APPDYNAMICS INJECTION
		if [ ! -d "#{temp}/appserveragent/" ]; then
			/usr/bin/unzip #{temp}/appserveragent.zip -d #{temp}/appserveragent/
		else
			echo "AppDynamics appserveragent.zip already deployed. Will not overwrite" >>/tmp/ad_install.log
		fi;
		if [ ! -d "#{temp}/machineagent/" ]; then
			/usr/bin/unzip #{temp}/machineagent.zip -d #{temp}/machineagent/
		else
			echo "AppDynamics machineagent.zip already deployed. Will not overwrite" >>/tmp/ad_install.log
		fi;
		echo "Running [`date`]..." >>/tmp/ad_install.log
		if [ `ps aux | grep machineagent | wc -l` -lt 2 ]; then 
			nohup java -jar #{temp}/machineagent/machineagent.jar &
		else
			echo "AppDynamics [MachineAgent] already started, will not recreate" >>/tmp/ad_install.log
		fi;

     EOF
end
