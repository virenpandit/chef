#
# Cookbook Name:: appdynamics
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'

temp=Chef::Config[:file_cache_path]

# Installs zip (through apt-get install)
package "zip" do
    action :install
end

if !File.exist?("#{temp}/appserveragent.zip")
    remote_file "#{temp}/appserveragent.zip" do
         source "http://offbelaycorp/appserveragent.zip"
         mode 00777
    end
end

if !File.exist?("#{temp}/appserveragent.zip")
    remote_file "#{temp}/machineagent.zip" do
         source "http://offbelaycorp/machineagent.zip"
         mode 00777
    end
end

bash 'setup_appdynamics' do
     code <<-EOF

        if [ ! -d "#{node['APPDYN_HOME']}" ]; then
            mkdir -p #{node['APPDYN_HOME']}
        fi

        if [ ! -d "#{node['APPDYN_HOME']}/appserveragent/" ]; then
            /usr/bin/unzip #{temp}/appserveragent.zip -d #{node['APPDYN_HOME']}/appserveragent/
        else
            echo "AppDynamics appserveragent.zip already deployed. Will not overwrite" >>/tmp/chefprovisioning.log
        fi;
        if [ ! -d "#{temp}/machineagent/" ]; then
            /usr/bin/unzip #{temp}/machineagent.zip -d #{node['APPDYN_HOME']}/machineagent/
        else
            echo "AppDynamics machineagent.zip already deployed. Will not overwrite" >>/tmp/chefprovisioning.log
        fi;

        if [ `ps aux | grep [m]achineagent | wc -l` -lt 1 ]; then 
            nohup java -jar #{node['APPDYN_HOME']}/machineagent/machineagent.jar 1>>#{node['APPDYN_HOME']}/appdynamics.log 2>&1 &
            echo "AppDynamics machineagent started on VM" >>/tmp/chefprovisioning.log
        else
            echo "AppDynamics machineagent already running, will not restart" >>/tmp/chefprovisioning.log
        fi;

     EOF
end
