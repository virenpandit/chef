#
# Cookbook Name:: selenium
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'

temp=Chef::Config[:file_cache_path]

# include_recipe 'maven'

package "xvfb" do
    action :install
end

package "xfonts-base" do
    action :install
end

package "xfonts-75dpi" do
    action :install
end

package "xfonts-100dpi" do
    action :install
end

package "firefox" do
    action :install
end

package "zip" do
    action :install
end

remote_file "#{temp}/selenium-server-with-deps.zip" do
     source "http://offbelaycorp/selenium/selenium-server-with-deps.zip"
     mode 00777
end

bash 'run_jar' do
     code <<-EOF

        echo "Selenium: Installing Server" >>/tmp/chefprovisioning.log
        unzip #{temp}/selenium-server-with-deps.zip -d /usr/local/selenium

        echo "Starting Selenium Hub..." >>/tmp/chefprovisioning.log
        nohup xvfb-run -e /tmp/seleniumserver.log java -jar /usr/local/selenium/selenium-server-standalone-2.38.0.jar -role hub 1>>/tmp/chefprovisioning.log 2>&1 &
        nohup Xvfb :7055 -screen 0 1024x768x24 &
        export DISPLAY=:7055
        echo "Starting Selenium Node..." >>/tmp/chefprovisioning.log
        nohup java -jar /usr/local/selenium/selenium-server-standalone-2.38.0.jar -role node -hub http://127.0.0.1:4444/wd/hub 1>>/tmp/chefprovisioning.log 2>&1 &
        echo "Selenium Grid started." >>/tmp/chefprovisioning.log

     EOF
end
