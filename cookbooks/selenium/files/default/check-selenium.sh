#!/bin/bash

if [ `ps aux | grep [s]elenium | wc -l` -gt 2 ] && [ `ps aux | grep [X]vfb | wc -l` -gt 1 ]; then 
    echo "running";
else
    echo "stopped";
fi;
