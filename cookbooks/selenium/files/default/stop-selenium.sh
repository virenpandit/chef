#!/bin/bash

for i in `ps -ef | grep [s]elenium-server | awk -F" " '{print $2}'`; do kill -9 $i 2>/dev/null; done;
for i in `ps -ef | grep [X]vfb | awk -F" " '{print $2}'`; do kill -9 $i 2>/dev/null; done;

