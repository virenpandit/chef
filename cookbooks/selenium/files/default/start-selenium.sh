#!/bin/bash

if [ `./check-selenium.sh` == "running" ]; then 
    echo "Error: Selenium Server already running. Use restart-selenium.sh if you want to restart it";
else
    echo "### Starting Selenium Hub on `date`..." >>/tmp/chefprovisioning.log
    nohup xvfb-run -e /tmp/seleniumserver.log java -jar /usr/local/selenium/selenium-server-standalone-2.38.0.jar -role hub 1>>/tmp/chefprovisioning.log 2>&1 &
    echo "Pausing for a few seconds for Selenium hub to start..." >>/tmp/chefprovisioning.log
    sleep 5;
    nohup Xvfb :7055 -screen 0 1024x768x24 1>>/tmp/chefprovisioning.log 2>&1 &
    export DISPLAY=:7055
    echo "Starting Selenium Node..." >>/tmp/chefprovisioning.log
    nohup java -jar /usr/local/selenium/selenium-server-standalone-2.38.0.jar -role node -hub http://127.0.0.1:4444/wd/hub 1>>/tmp/chefprovisioning.log 2>&1 &
    echo "Selenium Grid started." >>/tmp/chefprovisioning.log
fi;

