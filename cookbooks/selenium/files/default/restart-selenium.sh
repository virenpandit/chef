#!/bin/bash

echo "Stopping Selenium Server..." >>/tmp/chefprovisioning.log
./stop-selenium.sh
echo "Restarting Selenium Server..." >>/tmp/chefprovisioning.log
./start-selenium.sh
echo "Selenium Server Restarted" >>/tmp/chefprovisioning.log

