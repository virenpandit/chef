#!/bin/bash
# /etc/init.d/aem
# debian-compatible aem startup script.
#
### BEGIN INIT INFO
# Provides:          AEM
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start jenkins at boot time
# Description:       Controls the AEM Server
### END INIT INFO

# 1. CREATE THIS FILE UNDER /etc/rc.d/init.d/aem
# 2. CREATE A SYMBOLIC LINK UNDER THE APPROPRIATE RUN LEVEL E.G. Run-Level 7
# /etc/rc7.d/aem -> /etc/init.d/aem

PATH=/bin:/usr/bin:/sbin:/usr/sbin

DESC="Adobe AEM/CQ Server"
NAME=aem
SCRIPTNAME=/etc/init.d/$NAME

[ -r /etc/default/$NAME ] && . /etc/default/$NAME

SU=/bin/su

# load environments
if [ -r /etc/default/locale ]; then
  . /etc/default/locale
  export LANG LANGUAGE
elif [ -r /etc/environment ]; then
  . /etc/environment
  export LANG LANGUAGE
fi

log_daemon_msg() {
    echo $*
    echo $* >> /tmp/chefprovisioning.log
}

# Make sure we run as root, since setting the max open files through
# ulimit requires root access
if [ `id -u` -ne 0 ]; then
    log_daemon_msg "The $NAME init script can only be run as root"
    exit 1
fi

check_tcp_port() {
    port=4503
 
    count=`netstat --listen --numeric-ports | grep 4503 | grep -c . `

    if [ $count -ne 0 ]; then
        log_daemon_msg "The selected $service port ($port) seems to be in use by another program "
        log_daemon_msg "Please select another port to use for $NAME"
        return 1
    fi
}

#
# Function that starts the daemon/service
#
do_start()
{
    check_tcp_port || return 1
    log_daemon_msg "Starting AEM Publish Instance..."
    cd /opt/aem/publish/crx-quickstart;
    nohup bin/start 1>>/tmp/chefprovisioning.log 2>&1 &
}


#
# Verify that all AEM processes have been shutdown
# and if not, then do killall for them
# 
get_running() 
{
    return `ps aux | grep [4]502 | wc -l`
}

force_stop() 
{
    get_running
    if [ $? -ne 0 ]; then 
        ps -ef | grep [j]ava | grep 4503 | awk '{print substr($2,1,5),substr($3,1,5)}' | xargs kill 2>/dev/null || return 3
    fi
}

#
# Function that stops the daemon/service
#
do_stop()
{
    $SU -l root --shell=/bin/bash -c "cd /opt/aem/publish/crx-quickstart; bin/stop" || return 0
    force_stop

    # Many daemons don't delete their pidfiles when they exit.
    $SU -l root --shell=/bin/bash -c "rm -f /opt/aem/publish/crx-quickstart/conf/cq.pid"

    return 0
}

case "$1" in
  start)
    log_daemon_msg "Starting $DESC" "$NAME"
    do_start
    ;;
  stop)
    log_daemon_msg "Stopping $DESC" "$NAME"
    do_stop
    ;;
  restart)
    log_daemon_msg "Restarting $DESC" "$NAME"
    do_stop
    do_start
    ;;
  status)
    get_running
	case "$?" in 
	 0)
        log_daemon_msg "$DESC is down"
        exit 0
		;;
	 *)
        log_daemon_msg "$DESC is running with the pid `ps -ef | grep [j]ava | grep 4503 | awk '{print substr($2,1,5)}'`"
		exit 0
		;;
	esac
	;;
  *)
    log_daemon_msg "Usage: $SCRIPTNAME {start|stop|status|restart}" >&2
    exit 3
    ;;
esac

exit 0
