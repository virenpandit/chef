#
# Cookbook Name:: aempub
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'
include_recipe 'appdynamics'

temp=Chef::Config[:file_cache_path]

# Installs zip (through apt-get install)
package "zip" do
    action :install
end

log "### Platform=#{node['platform']}"

# Installs sshpass (through apt-get install)
if File.exist?("/etc/redhat-release")
    if !File.exist?("#{temp}/sshpass-1.05.tar.gz")
        remote_file "#{temp}/sshpass-1.05.tar.gz" do
             source "http://offbelaycorp/sshpass-1.05.tar.gz"
             mode 00777
        end
    end
    bash 'seutp_aem_publish' do
        code <<-EOF
            cd #{temp}
            tar xfvz sshpass-1.05.tar.gz
            cd sshpass-1.05 && ./configure && make && make install
        EOF
    end
else
    package "sshpass" do
        action :install
    end
end

if !File.exist?("#{temp}/AEM_5_6_Quickstart.jar")
    remote_file "#{temp}/AEM_5_6_Quickstart.jar" do
         source "http://offbelaycorp/aem/AEM_5_6_Quickstart.jar"
         mode 00777
    end
end

if !File.exist?("#{temp}/license.properties")
    remote_file "#{temp}/license.properties" do
         source "http://offbelaycorp/aem/license.properties"
         mode 00777
    end
end

log "### Running: seutp_aem_publish"

bash 'seutp_aem_publish' do
     code <<-EOF
        echo "AEM Installation log can be found at: /tmp/chefprovisioning.log"

        if [ ! -d "/opt/aem/publish" ]; then
            mkdir -p /opt/aem/publish;
        else
            echo "AEM publish /opt/aem/publish exists. Will not recreate" >>/tmp/chefprovisioning.log
        fi;
        if [ ! -f "/opt/aem/publish/cq5-publish-p4503.jar" ]; then
            mv #{temp}/AEM_5_6_Quickstart.jar /opt/aem/publish/cq5-publish-p4503.jar
        else
            echo "AEM Publish JAR exists. Will not replace" >>/tmp/chefprovisioning.log
        fi;
        if [ ! -f "/opt/aem/publish/license.properties" ]; then
            mv #{temp}/license.properties /opt/aem/publish/
        else
            echo "AEM license file exists. Will not replace" >>/tmp/chefprovisioning.log
        fi;
        if [ ! -d /opt/aem/publish/crx-quickstart ]; then
            echo "Unpacking AEM Jar file cq5-publish-p4503.jar..." >>/tmp/chefprovisioning.log
            cd /opt/aem/publish/; /usr/bin/java -jar cq5-publish-p4503.jar -unpack
        else
            echo "Will not unpack new AEM jar-file as directory publish/crx-quickstart already exists";
        fi;
     EOF
end

log "### Configuring AEM pre-startup"

if !File.exist?("/opt/aem/publish/crx-quickstart/sling.properties")
    cookbook_file "add_file_sling_properties" do
      source "sling.properties"
      path "/opt/aem/publish/crx-quickstart/sling.properties"
      action :create
    end
end

if !File.exist?("/etc/init.d/aem")
    if File.exist?("/etc/redhat-release")
        cookbook_file "register_aem_as_a_service" do
          source "aem_rhel.sh"
          path "/etc/init.d/aem"
          mode 0777
          action :create
        end
    else
        cookbook_file "register_aem_as_a_service" do
          source "aem_ubuntu.sh"
          path "/etc/init.d/aem"
          mode 0777
          action :create
        end
    end
end

cookbook_file "add_file_aem_start_command" do
  source "start.sh"
  path "/opt/aem/publish/crx-quickstart/bin/start"
  mode 0777
  action :create
end

log "### Registering AEM with Jenkins"

search(:node, "name:*jenkins*") do |matching_node|
  JENKINS_HOSTNAME = matching_node.to_s
  matching_node['network']["interfaces"]["eth1"]["addresses"].each do |ip, params|
      if params['family'] == ('inet')
          log "### Jenkins public IP Address is: #{ip}"
          JENKINS_IP = ip
      end
  end
end
log "### Jenkins server IP=#{JENKINS_IP}"

bash 'register_with_jenkins' do
    code <<-EOF
        if [ "$JENKINS_IP" != "" ]; then
            echo "Registering new AEM instance with Jenkins..." >>/tmp/chefprovisioning.log
            # JENKINS JOBS FOR NEW SERVER
            sshpass -p 'vagrant' ssh -o StrictHostKeyChecking=no vagrant@#{JENKINS_IP} sudo chef-client -S https://api.opscode.com/organizations/wht -o ci-aem
            aem_host=`hostname`;aem_ip=`ifconfig eth1 | grep "inet addr" | cut -d":" -f2 | cut -d" " -f1`;sshpass -p 'vagrant' ssh -o StrictHostKeyChecking=no vagrant@#{JENKINS_IP} "sudo /var/lib/jenkins/add-job.sh $aem_host $aem_ip #{temp}"
            sshpass -p 'vagrant' ssh -o StrictHostKeyChecking=no vagrant@#{JENKINS_IP} "sudo /etc/init.d/jenkins restart"
            echo "New AEM Server registration with Jenkins complete." >>/tmp/chefprovisioning.log
        else
            echo "No Jenkins server found by search. Bypassing registration of AEM with Jenkins" >> /tmp/chefprovisioning.log
        fi;
     EOF
end


log "### Creating a publish queue from Author server"
search(:node, "name:*aem*auth*") do |matching_node|
  AEM_PUB_HOSTNAME = matching_node.to_s
  matching_node['network']["interfaces"]["eth1"]["addresses"].each do |ip, params|
      if params['family'] == ('inet')
          log "### AEM Author public IP Address is: #{ip}"
          AEM_AUTH_IP = ip
      end
  end
end
log "### AEM Author server IP=#{AEM_AUTH_IP}"
bash 'create_publish_queue_on_author' do
    code <<-EOF
        AEM_PUB_IP=`ifconfig eth1 | grep "inet addr" | cut -d":" -f2 | cut -d" " -f1`
        echo "Creating Queue on Author instance: #{AEM_AUTH_IP} for Publish instance: $AEM_PUB_IP" >>/tmp/chefprovisioning.log
        curl -s -u admin:admin -X POST -F"cmd=createPage" -F"_charset_=utf-8" -F"parentPath=/etc/replication/agents.author" -F"title=DevPub01Agent" -F"label=DevPub01Agent" -F"template=/libs/cq/replication/templates/agent" "http://#{AEM_AUTH_IP}:4502/bin/wcmcommand"

        echo "Configuring Queue on Author instance: #{AEM_AUTH_IP} for Publish instance: $AEM_PUB_IP" >>/tmp/chefprovisioning.log
        curl -s -u admin:admin -X POST -F"./sling:resourceType=cq/replication/components/agent" -F"_charset_=utf-8" -F"./jcr:title=DevPub01Agent" -F"./jcr:description=description for agent DevPub01Agent" -F"./enabled=true" -F"./serializationType=durbo" -F"./retryDelay=60000" -F"./transportUri=http://$AEM_PUB_IP:4503/bin/receive" -F"./transportUser=admin" -F"./transportPassword=admin" -F"./protocolHTTPSRelaxed=true" -F"./protocolHTTPExpired=true" -F"./protocolHTTPConnectionClose@Delete=true" "http://#{AEM_AUTH_IP}:4502/etc/replication/agents.author/DevPub01Agent/jcr:content"
    EOF
end

log "### Starting up AEM Server"

bash 'start_aem_server' do
    code <<-EOF
        echo "Starting AEM Publish server..." >>/tmp/chefprovisioning.log
        /etc/init.d/aem start 1>>/tmp/chefprovisioning.log 2>&1
        echo "AEM Publish Server started successfully." >>/tmp/chefprovisioning.log
    EOF
end
