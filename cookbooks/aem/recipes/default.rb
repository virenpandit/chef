#
# Cookbook Name:: aem
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'

temp=Chef::Config[:file_cache_path]

if !File.exist?("#{temp}/AEM_5_6_Quickstart.jar")
	remote_file "#{temp}/AEM_5_6_Quickstart.jar" do
         source "http://offbelaycorp/aem/AEM_5_6_Quickstart.jar"
		 mode 00777
	end
end

remote_file "#{temp}/license.properties" do
     source "http://offbelaycorp/aem/license.properties"
     mode 00777
end

remote_file "#{temp}/appserveragent.zip" do
     source "http://offbelaycorp/appserveragent.zip"
     mode 00777
end

remote_file "#{temp}/machineagent.zip" do
     source "http://offbelaycorp/machineagent.zip"
     mode 00777
end
