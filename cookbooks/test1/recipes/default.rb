#
# Cookbook Name:: test1
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

temp=node['hostname']

log "### Platform=#{node['platform']}"

puts "### Hello World from PUTS"
Chef::Log.info("### Hello World from CHEF:LOG:INFO")

Chef::Log.info "Hello World Again: hostname=#{node['hostname']}, IP=#{node['ipaddress']}"

Chef::Log.info "Default CQ_RUNMODE=#{node.default['CQ_RUNMODE']}"

Chef::Log.info "Normal SERVER=#{node.normal['SERVER']}"

Chef::Log.info "Before: Attribute T2TEST=#{node.normal['T2TEST']}"
node.set["T2TEST"] = "Test2.1"
Chef::Log.info "After: Attribute T2TEST=#{node.normal['T2TEST']}"

include_recipe 'ohai::default'

template "/tmp/my-config.xml" do
  source "my-config.xml.erb"
  mode 0440
  owner "root"
  group "root"
  variables({
     :names => "VirenPandit"
  })
end

directory "/tmp/foo/bar/moby/dick" do
  owner "root"
  group "root"
  mode 00644
  action :create
  recursive true
end

# Will fail after first execution, as directory was created
# execute "mkdir /tmp/executed"

if File.exist?("/tmp/executed2")
	execute "rm -rf /tmp/executed2"
end

if !File.exist?("/tmp/executed3")
	execute "mkdir /tmp/executed3"
end

if !File.exist?("/tmp/executed4")
	execute "I_want_to_do_something" do
		command "mkdir /tmp/executed4"
	end
end

#execute "I_want_to_do_something_more" do
#	not_if File.exist?("mkdir /tmp/executed5")
#	command "mkdir /tmp/executed5"
#end

# Installs zip (through apt-get install)
package "zip" do
	action :install
end


log "your string to log"


log "another debug string" do
  level :info
end

remote_directory "/tmp/testdir" do
  source "testdir"
  action :create_if_missing
end


# To Add a Chef file to server: 
# Ensure file add-job.sh exists under files/default/add-job.sh
cookbook_file "add_file_add_job_sh" do
  source "add-job.sh"
  path "/var/lib/jenkins/add-job.sh"
  mode 0777
  action :create
end



log "Attribute=#{node['jenkins']['server']['home']}"


node['install_sites'].each do |site|
  Chef::Log.info "###a) site=#{site}"
end


node['install_sites_with_ports'].each do |site,port|
  Chef::Log.info "###b) site=#{site}, port=#{port}"
end


node['install_sitesobjects'].each do |site,wlsserver|
  Chef::Log.info "###c) site=#{site}, server=#{wlsserver.servername}, port=#{wlsserver.serverport}"
end

