
# Be sure to include dependency on Jenkins in metadata.rb first

include_attribute "jenkins::server"

default['CQ_RUNMODE'] = 'publish'

normal['SERVER'] = 'AEM'

normal['T2TEST'] = 'Test1.1'




default['install_sites'] = ["siteA", "siteB", "siteC"]

default['install_sites_with_ports'] = { "siteA" => "7101", "siteB" => "7102", "siteC" => "7103" }

class WLSServer
    attr_accessor :servername, :serverport

    def initialize(name, port)
        @servername = name
        @serverport = port
    end
end
default['install_sitesobjects'] = { "siteAo" => WLSServer.new("siteAoo", "7101oo"), "siteBo" => WLSServer.new("siteBoo", "7102oo") }
