#!/bin/bash

USAGE_STRING="add-job.sh <hostname> <ip-for-hostname> <Chef File cache path>"

JENKINS_HOME=/var/lib/jenkins
MAVEN_HOME=/usr/local/maven

REGISTRATION_FILE=$JENKINS_HOME/registered-aem-hosts.lst

function logInfo {
    echo $* >> /tmp/chefprovisioning.log
}
function logError {
    echo "ERROR: $*" >> /tmp/chefprovisioning.log
}
function errorOut {
    return 1;
}

if [ "$3" != "" ]; then 
    CHEF_FILE_STORE=$3
    touch $REGISTRATION_FILE
    chmod 777 $REGISTRATION_FILE
    if [ `cat $REGISTRATION_FILE | grep "$2$" | wc -l` -eq "0" ]; then
        echo "$1,$2">>$REGISTRATION_FILE
        logInfo "New AEM Host Registered"
    fi;
    cat $REGISTRATION_FILE | while read line; do
        hostname=`echo $line | cut -d"," -f1`
        host_ipaddress=`echo $line | cut -d"," -f2`

        if [ ! -f $CHEF_FILE_STORE/deploy-config.xml ]; then
            logError "No such file: $CHEF_FILE_STORE/deploy-config.xml"
            errorOut;
        else
            if [ -d $JENKINS_HOME/jobs/aem-dev-deploy-$hostname ]; then
                logError "Deploy job already exists: $JENKINS_HOME/jobs/aem-dev-deploy-$hostname"
                logError "Will not overwrite $JENKINS_HOME/jobs/aem-dev-deploy-$hostname, continuing..."
            else
                cp $CHEF_FILE_STORE/deploy-config.xml /tmp/deploy-config-$hostname.xml
                perl -pi -e "s/CHEF_MGD_CQSERVER/$host_ipaddress/g" /tmp/deploy-config-$hostname.xml;
                mkdir -p $JENKINS_HOME/jobs/aem-dev-deploy-$hostname;
                mv /tmp/deploy-config-$hostname.xml $JENKINS_HOME/jobs/aem-dev-deploy-$hostname/config.xml;
                chown -R jenkins:jenkins $JENKINS_HOME/jobs/aem-dev-deploy-$hostname
                logInfo "New Job added: $JENKINS_HOME/jobs/aem-dev-deploy-$hostname"

                logInfo "Uploading Archiva from: $CHEF_FILE_STORE/cq5-archiva-servlet-1.5.zip to CQ5 instance: $host_ipaddress"
                curl --output /tmp/curlupload.log --trace /tmp/curlupload.log --stderr /tmp/curlupload.log -u admin:admin -Fpackage=@$CHEF_FILE_STORE/cq5-archiva-servlet-1.5.zip http://$host_ipaddress:4502/crx/packmgr/service/.json/?cmd=upload
                curl --output /tmp/curlinstall.log --trace /tmp/curlinstall.log --stderr /tmp/curlinstall.log -u admin:admin -X POST http://$host_ipaddress:4502/crx/packmgr/service/.json/etc/packages/cq5-archiva-servlet-1.5.zip?cmd=install

                cp $CHEF_FILE_STORE/maven-settings.xml /tmp/maven-settings.xml
                perl -pi -e "s/CHEF_MGD_CQSERVER/$host_ipaddress/g" /tmp/maven-settings.xml;
                mv /tmp/maven-settings.xml $MAVEN_HOME/conf/settings.xml
                logInfo "New maven-settings.xml file created at location $MAVEN_HOME/conf/"
            fi;
        fi;
    done;
else
    logError "Not Enough Arguments"
    logError "Example: $USAGE_STRING"
    errorOut
fi;

