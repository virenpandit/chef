name             'wyn01'
maintainer       'WYNDHAM'
maintainer_email 'viren.pandit@wyn.com'
license          'All rights reserved'
description      'Installs/Configures Wyndham Java Development Server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends 'java'
