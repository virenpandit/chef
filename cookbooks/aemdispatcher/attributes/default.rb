
include_attribute "apache2::default"

default['ADMIN_EMAIL'] = 'admin@wyndhamworldwide.com'

default['AEM_SERVERNAME'] = 'fqa-aem-dispatcher'

default['apache']['listen_ports'] = %w[80]
