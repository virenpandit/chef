#
# Cookbook Name:: aemdispatcher
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apache2'

temp=Chef::Config[:file_cache_path]

log "### Apache installation directory: #{node['apache']['dir']}"
log "### Apache DocRoot: #{node['apache']['docroot_dir']}"
log "### Apache Modules Directory: #{node['apache']['libexecdir']}"

log "Creating AEM Dispatcher cache directories..."
bash 'create_dispatcher_cache_directories' do
    code <<-EOF
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/daysinn-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/daysinn-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/super8-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/super8-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/hojo-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/hojo-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/baymontinns-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/baymontinns-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/dreamhotels-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/dreamhotels-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/wingatehotels-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/wingatehotels-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/wyndhamhotelgroup-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/wyndhamhotelgroup-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/knightsinn-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/knightsinn-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/travelodge-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/travelodge-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/hawthorn-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/hawthorn-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/wyndham-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/wyndham-mobile; fi;
        if [ ! -d #{node['apache']['docroot_dir']}/dispatcher-cache/microtelinn-mobile ]; then mkdir -p #{node['apache']['docroot_dir']}/dispatcher-cache/microtelinn-mobile; fi;
        
        touch #{node['apache']['dir']}/conf/mime.types
    EOF
end

log "Copying AEM Dispatcher Module [mod_dispatcher.so]"
if !File.exist?("#{node['apache']['libexecdir']}/mod_dispatcher.so")
    remote_file "#{node['apache']['libexecdir']}/mod_dispatcher.so" do
         source "http://offbelaycorp/aem/dispatcher-apache2.2-4.1.5.so"
         mode 00777
    end
end

log "Copying AEM Dispatcher Config [dispatcher.any]"
if !File.exist?("#{node['apache']['dir']}/conf/dispatcher.any")
    template "#{node['apache']['dir']}/conf/dispatcher.any" do
      source "dispatcher.any.erb"
      mode 0644
      owner "root"
      group "root"
    end
end

log "Updating httpd.conf"
    template "#{node['apache']['dir']}/conf/httpd.conf" do
      source "httpd.conf.erb"
      mode 0644
      owner "root"
      group "root"
    end

log "Searching for matching publish servers.."
search(:node, "name:*aem*auth*") do |matching_node|
  AEM_PUB_HOSTNAME = matching_node.to_s
  matching_node['network']["interfaces"]["eth1"]["addresses"].each do |ip, params|
      if params['family'] == ('inet')
          log "AEM Publish public IP Address is: #{ip}"
          AEM_PUB_IP = ip
      end
  end
end
log "AEM Publish server IP=#{AEM_PUB_IP}"



log "Restarting Apache server..."
bash 'restart_apache2_daemon' do
    code <<-EOF
        service httpd restart
    EOF
end
