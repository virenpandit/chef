#
# Cookbook Name:: weblogic
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'

temp=Chef::Config[:file_cache_path]

template "#{temp}/wls-silent-install.xml" do
  source "wls-silent-install.xml.erb"
  mode 0440
  owner "root"
  group "root"
end

if !File.exist?("#{temp}/wls1036_generic.jar")
	puts "Downloading WebLogic Installer [wls1036_generic.jar]..."
	remote_file "#{temp}/wls1036_generic.jar" do
         source "http://offbelaycorp/weblogic/wls1036_generic.jar"
		 mode 00777
	end
end


if !Dir.exist?("#{node['WL_HOME']}")
	puts "Installing Weblogic 10.3.6 to directory: #{node.default['WL_HOME']}"
	execute "install_wls" do
        command "java -jar #{temp}/wls1036_generic.jar -mode=silent -silent_xml=#{temp}/wls-silent-install.xml -silent_log=/tmp/chefprovisioning.log -log=/tmp/chefprovisioning.log"
	end
end
