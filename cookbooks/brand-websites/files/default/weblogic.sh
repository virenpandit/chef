#!/bin/bash
# /etc/init.d/aem
# debian-compatible aem startup script.
#
### BEGIN INIT INFO
# Provides:          AEM
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start WebLogic at boot time

get_running() { 
    ps aux | grep [w]eblogic.Server 1>/dev/null 2>&1
    return $?
}
do_start() {
    get_running && echo "WebLogic Server already running" && return 1
    echo "Starting WebLogic Server..."
    cd /opt/weblogic/domains/branddomain
    nohup ./startWebLogic.sh &
    return 0
}

do_stop() {
    get_running && echo "Stopping WebLogic Server" && ps aux | grep [w]eblogic.Server | awk '{print substr($2,1,5)}' | xargs kill
    return 0
}

case "$1" in
  start)
    do_start
    ;;
  stop)
    do_stop
    ;;
  restart)
    do_stop
    do_start
    ;;
  status)
    get_running
    case "$?" in
        0)
        echo "WebLogic Server is running"
        ;;
        *)
        echo "WebLogic Server is not running"
        exit 1
        ;;
     esac
    ;;
  *)
    echo "Usage: $SCRIPTNAME {start|stop|status|restart}" >&2
    exit 3
    ;;
esac
exit 0
