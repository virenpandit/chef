#
# Cookbook Name:: brand-websites
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'weblogic'

# Installs zip (through apt-get install)
package "zip" do
    action :install
end

temp=Chef::Config[:file_cache_path]

template "#{temp}/create_standalone_domain.py" do
  source "create_standalone_domain.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/check_server_startup.py" do
  source "check_server_startup.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/deploy_deps_war.py" do
  source "deploy_deps_war.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/createJDBCDS.py" do
  source "createJDBCDS.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/createMultiDS.py" do
  source "createMultiDS.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/deploy_brand_war.py" do
  source "deploy_brand_war.py.erb"
  mode 0440
  owner "root"
  group "root"
end

template "#{temp}/disableAutoDeploy.py" do
  source "disableAutoDeploy.py.erb"
  mode 0440
  owner "root"
  group "root"
end

remote_file "/tmp/brand.war" do
     source "http://offbelaycorp/brandcom/brand.war"
     mode 00777
end

log "Copying JMS config to domain"
remote_directory "#{temp}/jms" do
  source "jms"
  action :create_if_missing
end

node['install_sitesobjects'].each do |site,wlsserver|
    log "Creating Site: server=#{wlsserver.servername}, port=#{wlsserver.serverport}"

    template "#{temp}/jms-config-part-#{wlsserver.servername}.xml" do
      source "jms-config-part.xml.erb"
      mode 0440
      owner "root"
      group "root"
      variables({
         :servername => "#{wlsserver.servername}"
      })
    end

	bash "create_brand_domain" do
      code <<-EOH
      
        echo "### WLS: Starting new WebLogic Server deployment" >>/tmp/chefprovisioning.log
        
        if [ ! -d "#{node['WL_HOME']}/domains/branddomain" ]; then
            echo "### WLS: Creating domain: branddomain" >>/tmp/chefprovisioning.log
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/create_standalone_domain.py #{wlsserver.servername} #{wlsserver.serverport} 1>>/tmp/chefprovisioning.log 2>&1
        else
            echo "### WLS: branddomain already exists, will not recreate. Delete domain first if you wish to proceed" >>/tmp/chefprovisioning.log
        fi

      EOH
    end

    if !File.exist?("/etc/init.d/weblogic")
        cookbook_file "register_wls_as_a_service" do
          source "weblogic.sh"
          path "/etc/init.d/weblogic"
          mode 0777
          action :create
        end
    end

    bash "create_jms_queues" do
      code <<-EOH

        if [ `grep "<jms-server>" #{node['WL_HOME']}/domains/branddomain/config/config.xml | wc -l` -eq 0 ]; then
            echo "### WLS: Creating JMS queues";  >>/tmp/chefprovisioning.log
            cat #{node['WL_HOME']}/domains/branddomain/config/config.xml #{temp}/jms-config-part-#{wlsserver.servername}.xml | grep -v "</domain>" | grep -v "<admin-server-name>" > #{temp}/with_jms_config.xml; echo "<admin-server-name>#{wlsserver.servername}server</admin-server-name></domain>" >> #{temp}/with_jms_config.xml
            mv #{node['WL_HOME']}/domains/branddomain/config/config.xml #{node['WL_HOME']}/domains/branddomain/config/config-original.bak
            cp #{temp}/with_jms_config.xml #{node['WL_HOME']}/domains/branddomain/config/config.xml
            cp -r #{temp}/jms #{node['WL_HOME']}/domains/branddomain/config/
        else
            echo "### WLS: JMS configuration already exists in config.xml. Will not overwrite/add new" >>/tmp/chefprovisioning.log
        fi;
      EOH
    end

    bash "start_wls_server" do
      code <<-EOH
        service weblogic start

        _MAX=5;_ctr=0;_flag=1;
        while (($_flag==1 && _ctr<$_MAX)); do 
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/check_server_startup.py #{wlsserver.serverport}; 
            _flag=$?; 
            _ctr=$(($_ctr+1));
            echo "### WLS: Waiting for Weblogic server to start..." >>/tmp/chefprovisioning.log
            sleep 3; 
        done;
      EOH
    end

    bash "configure_and_deploy_brand_com" do
      code <<-EOH
        service weblogic status
        if (($? != 0)); then 
            echo "### WLS: Unable to connect to server, Server probably did not startup properly. Giving up after $_MAX tries";  >>/tmp/chefprovisioning.log
        else
            echo "### WLS: WebLogic Server started successfully. Tuning for faster console load..." >>/tmp/chefprovisioning.log
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/disableAutoDeploy.py #{wlsserver.serverport} 1>>/tmp/chefprovisioning.log 2>&1

            echo "### WLS: Tuning complete. Deploying dependencies for brand.com..." >>/tmp/chefprovisioning.log
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/deploy_deps_war.py #{wlsserver.servername} #{wlsserver.serverport} 1>>/tmp/chefprovisioning.log 2>&1

            echo "### WLS: Creating JDBC DataSources..." >>/tmp/chefprovisioning.log
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/createJDBCDS.py #{wlsserver.serverport} 1>>/tmp/chefprovisioning.log 2>&1
            echo "### WLS: Creating JDBC Multi-datasource..." >>/tmp/chefprovisioning.log
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/createMultiDS.py #{wlsserver.serverport} #{node['jdbcMultiDS1Name']} #{node['jdbcMultiDS1JndiName']} 1>>/tmp/chefprovisioning.log 2>&1
            #{node['WL_HOME']}/wlserver_10.3/common/bin/wlst.sh #{temp}/createMultiDS.py #{wlsserver.serverport} #{node['jdbcMultiDS2Name']} #{node['jdbcMultiDS2JndiName']} 1>>/tmp/chefprovisioning.log 2>&1

            echo "### WLS Server up. Dependencies for Brand.com deployed, JMS queues should be created and JDBC Datasources should also be created" >>/tmp/chefprovisioning.log

            echo "### WLS Server up. Deploying [Brand].com" >>/tmp/chefprovisioning.log
          # #{node['WL_HOME']}/wlserver_10.3/common/bin/__wlst.sh #{temp}/deploy_brand_war.py #{wlsserver.servername} #{wlsserver.serverport} 1>>/tmp/chefprovisioning.log 2>&1
            cp /tmp/brand.war #{node['WL_HOME']}/domains/branddomain/autodeploy/brand.war
            echo "### WLS: Deployed Brand.com. Please restart the server" >>/tmp/chefprovisioning.log
        fi;
	  EOH
    end

end
