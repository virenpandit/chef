
include_attribute "weblogic"

default['WL_PORT'] = '7001'
default['WL_PORT_SSL'] = '7002'
default['WL_USER'] = 'weblogic'
default['WL_USER_PASS'] = 'webl0gic'


class WLSServer
    attr_accessor :servername, :serverport
    def initialize(name, port)
        @servername = name
        @serverport = port
    end
end
default['install_sitesobjects'] = { "site1" => WLSServer.new("brand", "7101") }

default['jdbcDSName'] = 'shopper_devint1'
default['jdbcDSJndiName'] = 'shopdevint1'
default['jdbcDSURL'] = 'jdbc:oracle:thin:@vsvphxshopdev01.hotelgroup.com:1521:shopintdev'
default['jdbcDSDriverName'] = 'oracle.jdbc.xa.client.OracleXADataSource'
default['jdbcDSUserName'] = 'SHOP_BWS_APP'
default['jdbcDSPassword'] = 'blue'
default['jdbcDSDatabaseName'] = 'shopintdev'
default['jdbcDSTarget'] = 'brandserver'

default['jdbcMultiDS1Name']='shopper2-datasource1'
default['jdbcMultiDS1JndiName'] = 'shop1'
default['jdbcMultiDSList']='shopper_devint1'
default['jdbcMultiDS2Name']='shopper2-datasource2'
default['jdbcMultiDS2JndiName'] = 'shop2'
default['jdbcMultiDSList']='shopper_devint1'
