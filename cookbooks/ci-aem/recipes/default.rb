#
# Cookbook Name:: ci-aem
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


temp=Chef::Config[:file_cache_path]

remote_file "#{temp}/cq5-archiva-servlet-1.5.zip" do
     source "http://offbelaycorp/aem/cq5-archiva-servlet-1.5.zip"
     mode 00777
end

template "#{temp}/checkout-config.xml" do
  source "checkout-config.xml.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end

template "#{temp}/build-config.xml" do
  source "build-config.xml.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end

template "#{temp}/deploy-config.xml" do
  source "deploy-config.xml.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end

template "#{temp}/maven-settings.xml" do
  source "maven-settings.xml.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end

template "#{temp}/findbugs-config.xml" do
  source "findbugs-config.xml.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end


log "### Jenkins Home=#{node['jenkins']['server']['home']}"
log "### Installing script add-job.sh to Jenkins for remote job additions"
template "#{node['jenkins']['server']['home']}/add-job.sh" do
  source "add-job.sh.erb"
  mode 0777
  owner "jenkins"
  group "jenkins"
end

log "### Creating Jenkins Jobs for AEM..."
bash 'create_jenkins_wkspc_and_jobs' do
     code <<-EOF

        if [ ! -d "#{node['JENKINS_WORKSPACE']}" ]; then 
            mkdir -p #{node['JENKINS_WORKSPACE']}; 
		fi;

        if [ ! -d "#{node['jenkins']['server']['home']}/jobs/aem-dev-checkout" ]; then 
            mkdir -p #{node['jenkins']['server']['home']}/jobs/aem-dev-checkout; 
            mv #{temp}/checkout-config.xml #{node['jenkins']['server']['home']}/jobs/aem-dev-checkout/config.xml;
		fi;

        if [ ! -d "#{node['jenkins']['server']['home']}/jobs/aem-dev-build" ]; then 
            mkdir -p #{node['jenkins']['server']['home']}/jobs/aem-dev-build; 
            mv #{temp}/build-config.xml #{node['jenkins']['server']['home']}/jobs/aem-dev-build/config.xml
		fi;

        if [ ! -d "#{node['jenkins']['server']['home']}/jobs/aem-dev-findbugs" ]; then 
            mkdir -p #{node['jenkins']['server']['home']}/jobs/aem-dev-findbugs; 
            mv #{temp}/findbugs-config.xml #{node['jenkins']['server']['home']}/jobs/aem-dev-findbugs/config.xml;
		fi;

        chown -R jenkins:jenkins #{node['jenkins']['server']['home']}
        chown -R jenkins:jenkins #{node['JENKINS_WORKSPACE']}/..
    EOF
end

log "### Restarting Jenkins server..."
basn 'restart_jenkins' do
    code <<-EOF
	 	/etc/init.d/jenkins restart
     EOF
end
