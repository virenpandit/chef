
include_attribute "jenkins::server"
include_attribute "maven::default"

default['JENKINS_WORKSPACE'] = '/tmp/jenkins/workspace'

default['ACCUREV_SERVER'] = 'svnjscmprx01'
default['ACCUREV_DEPOT'] = 'AEM_JAVA'
default['ACCUREV_STREAM'] = 'AEM_N.2013.10_VIRENPOC_S2'
default['ACCUREV_WKSPC'] = 'AEM_N.2013.10_DEV_STREAM_WORKSPACE2_pandivir'
default['AEM_SERVER_DEFAULT_PORT'] = '4502'
