#
# Cookbook Name:: accurev
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


remote_file "#{Chef::Config[:file_cache_path]}/AccuRev_5_5_0d_LinuxClientOnly_x86_2_4.bin" do
    source "http://offbelaycorp/accurev/AccuRev_5_5_0d_LinuxClientOnly_x86_2_4.bin"
	mode 00777
end

remote_file "#{Chef::Config[:file_cache_path]}/accurev_installer.properties" do
    source "http://offbelaycorp/accurev/accurev_installer.properties"
	mode 00777
end


bash 'run_jar' do
     code <<-EOF

		if [ ! -d "/opt/accurev/client" ]; then 
			echo "Installing Accurev" >> /tmp/jenkins_install.log
			#{Chef::Config[:file_cache_path]}/AccuRev_5_5_0d_LinuxClientOnly_x86_2_4.bin -i silent -f #{Chef::Config[:file_cache_path]}/accurev_installer.properties
		else
			echo "Accurev already installed" >> /tmp/accurev_install.log
		fi;
     EOF
end
